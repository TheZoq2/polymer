use core::cell::RefCell;

use cortex_m::interrupt::{
    self,
    Mutex,
};

use embedded_executor::{
    AllocExecutor,
    Sleep,
    Wake,
};

use crate::mutex::IFree;

static WOKEN: Mutex<RefCell<bool>> = Mutex::new(RefCell::new(false));

#[derive(Clone, Copy, Default)]
pub struct WFI;

impl Sleep for WFI {
    fn sleep(&self) {
        interrupt::free(|cs| {
            let mut woken = WOKEN.borrow(cs).borrow_mut();
            if !*woken {
                cortex_m::asm::wfi();
                *woken = true;
            }
        })
    }
}

impl Wake for WFI {
    fn wake(&self) {
        interrupt::free(|cs| {
            let mut woken = WOKEN.borrow(cs).borrow_mut();
            *woken = true;
        })
    }
}

pub type Executor<'a> = AllocExecutor<'a, IFree, WFI>;
