use keebrs::{
    keycode::KeyCode::*,
    translate::{
        Action::{
            self,
            *,
        },
        Board,
        Layout,
        Map,
        Orientation,
    },
};

const POLYMER: Action = Seq(&[
    Combo(&[KbLShift, KbP]),
    Key(KbO),
    Key(KbL),
    Key(KbY),
    Key(KbM),
    Key(KbE),
    Key(KbR),
    Key(KbEnter),
]);

fn bootloader() {
    // At worst, this'll just reboot the board if the bkp isn't enabled
    unsafe {
        crate::reboot::reboot_bootloader(core::mem::transmute(()));
    }
}

#[rustfmt::skip]
const LEFT_0: Map = Map(&[
    &[Key(KbIns),      Key(KbGrave),  Key(Kb1),    Key(Kb2),     Key(Kb3),    Key(Kb4), Key(Kb5)    ],
    &[Key(KbHyphen),   Key(KbTab),    Key(KbQ),    Key(KbW),     Key(KbE),    Key(KbR), Key(KbT)    ],
    &[Key(KbEq),       Key(KbEscape), Key(KbA),    Key(KbS),     Key(KbD),    Key(KbF), Key(KbG)    ],
    &[Key(KbPageUp),   Key(KbLShift), Key(KbZ),    Key(KbX),     Key(KbC),    Key(KbV), Key(KbB)    ],
    &[Key(KbPageDown), Layer(3),      Key(KbLCtrl), Key(KbLAlt), Key(KbLGui), Layer(1), Key(KbSpace)],
]);

#[rustfmt::skip]
const RIGHT_0: Map = Map(&[
    &[Key(Kb6),     Key(Kb7), Key(Kb8),       Key(Kb9),       Key(Kb0),     Key(KbBackspace), Key(KbDelete)   ],
    &[Key(KbY),     Key(KbU), Key(KbI),       Key(KbO),       Key(KbP),     Key(KbLBrace),    Key(KbRBrace)   ],
    &[Key(KbH),     Key(KbJ), Key(KbK),       Key(KbL),       Key(KbSemi),  Key(KbQuote),     Key(KbEnter)    ],
    &[Key(KbN),     Key(KbM), Key(KbComma),   Key(KbPeriod),  Key(KbSlash), Key(KbHome),      Key(KbEnd)      ],
    &[Key(KbSpace), Layer(2), Key(KbLeftArr), Key(KbDownArr), Key(KbUpArr), Key(KbRightArr),  Key(KbBackSlash)],
]);

const TILDE: Action = Combo(&[KbLShift, KbGrave]);
const USCORE: Action = Combo(&[KbLShift, KbHyphen]);
const PLUS: Action = Combo(&[KbLShift, KbEq]);
const BANG: Action = Combo(&[KbLShift, Kb1]);
const AT: Action = Combo(&[KbLShift, Kb2]);
const HASH: Action = Combo(&[KbLShift, Kb3]);
const DOLLAR: Action = Combo(&[KbLShift, Kb4]);
const PERCENT: Action = Combo(&[KbLShift, Kb5]);
const CARAT: Action = Combo(&[KbLShift, Kb6]);
const AMP: Action = Combo(&[KbLShift, Kb7]);
const STAR: Action = Combo(&[KbLShift, Kb8]);
const LPAREN: Action = Combo(&[KbLShift, Kb9]);
const RPAREN: Action = Combo(&[KbLShift, Kb0]);
const RBRACK: Action = Combo(&[KbLShift, KbLBrace]);
const LBRACK: Action = Combo(&[KbLShift, KbRBrace]);

#[rustfmt::skip]
const LEFT_1: Map = Map(&[
    &[Nop,      TILDE,          Key(KbF1),         Key(KbF2),       Key(KbF3),      Key(KbF4),  Key(KbF5) ],
    &[USCORE,   Nop,            BANG,              AT,              HASH,           DOLLAR,     PERCENT   ],
    &[PLUS,     Nop,            Key(KbF1),         Key(KbF2),       Key(KbF3),      Key(KbF4),  Key(KbF5) ],
    &[Nop,      Nop,            Key(KbF7),         Key(KbF8),       Key(KbF9),      Key(KbF10), Key(KbF11)],
    &[Layer(3), Key(MediaNext), Key(MediaVolDown), Key(MediaVolUp), Key(MediaPlay), Trans,      Nop       ],
]);

#[rustfmt::skip]
const RIGHT_1: Map = Map(&[
    &[Key(KbF6),  Key(KbF7), Key(KbF8),      Key(KbF9),         Key(KbF10),      Key(KbF11),      Key(KbF12)],
    &[CARAT,      AMP,       STAR,           LPAREN,            RPAREN,          LBRACK,          RBRACK    ],
    &[Key(KbF6),  USCORE,    PLUS,           LBRACK,            RBRACK,          Nop,             Nop       ],
    &[Key(KbF12), Nop,       Nop,            Nop,               Nop,             Nop,             Nop       ],
    &[Nop,        Trans,     Key(MediaNext), Key(MediaVolDown), Key(MediaVolUp), Key(MediaPlay),  Layer(3)  ],
]);

#[rustfmt::skip]
const LEFT_2: Map = Map(&[
    &[Nop,      TILDE,          Key(KbF1),         Key(KbF2),       Key(KbF3),      Key(KbF4),  Key(KbF5) ],
    &[USCORE,   Nop,            Key(Kb1),          Key(Kb2),        Key(Kb3),       Key(Kb4),   Key(Kb5)  ],
    &[PLUS,     Nop,            Key(KbF1),         Key(KbF2),       Key(KbF3),      Key(KbF4),  Key(KbF5) ],
    &[Nop,      Nop,            Key(KbF7),         Key(KbF8),       Key(KbF9),      Key(KbF10), Key(KbF11)],
    &[Layer(3), Key(MediaNext), Key(MediaVolDown), Key(MediaVolUp), Key(MediaPlay), Trans,      Nop       ],
]);

#[rustfmt::skip]
const RIGHT_2: Map = Map(&[
    &[Key(KbF6),  Key(KbF7),     Key(KbF8),      Key(KbF9),         Key(KbF10),      Key(KbF11),     Key(KbF12)    ],
    &[Key(Kb6),   Key(Kb7),      Key(Kb8),       Key(Kb9),          Key(Kb0),        LBRACK,         RBRACK        ],
    &[Key(KbF6),  Key(KbHyphen), Key(KbEq),      Key(KbLBrace),     Key(KbRBrace),   Nop,            Nop           ],
    &[Key(KbF12), Nop,           Nop,            Nop,               Nop,             Nop,            Nop           ],
    &[Nop,        Trans,         Key(MediaNext), Key(MediaVolDown), Key(MediaVolUp), Key(MediaPlay), Layer(3)      ],
]);

#[rustfmt::skip]
const LEFT_3: Map = Map(&[
    &[POLYMER, Nop, Nop, Nop, Nop, Nop, Fn(bootloader)],
    &[Nop,     Nop, Nop, Nop, Nop, Nop, Nop           ],
    &[Nop,     Nop, Nop, Nop, Nop, Nop, Nop           ],
    &[Nop,     Nop, Nop, Nop, Nop, Nop, Nop           ],
    &[Trans,   Nop, Nop, Nop, Nop, Nop, Nop           ],
]);

#[rustfmt::skip]
const RIGHT_3: Map = Map(&[
    &[Fn(bootloader), Nop, Nop, Nop, Nop, Nop, Nop  ],
    &[Nop,            Nop, Nop, Nop, Nop, Nop, Nop  ],
    &[Nop,            Nop, Nop, Nop, Nop, Nop, Nop  ],
    &[Nop,            Nop, Nop, Nop, Nop, Nop, Nop  ],
    &[Nop,            Nop, Nop, Nop, Nop, Nop, Trans],
]);

const LEFT: Board = Board {
    layers: &[LEFT_0, LEFT_1, LEFT_2, LEFT_3],
    orientation: Orientation::RC,
};

const RIGHT: Board = Board {
    layers: &[RIGHT_0, RIGHT_1, RIGHT_2, RIGHT_3],
    orientation: Orientation::RC,
};

pub const LAYOUT: Layout = Layout {
    boards: &[LEFT, RIGHT],
};
