#![feature(alloc_error_handler, alloc_prelude, never_type, const_fn)]
#![feature(generators)]
#![no_std]
#![no_main]

extern crate alloc;

#[cfg(feature = "semihosting")]
extern crate panic_semihosting;

#[cfg(all(not(feature = "itm"), not(feature = "semihosting")))]
extern crate panic_abort;

#[cfg(feature = "itm")]
extern crate panic_itm;

#[cfg(all(feature = "itm", feature = "semihosting"))]
compile_error!("can't enable itm and semihosting simultaneously");

#[macro_use]
mod allocator;
#[macro_use]
mod matrix;

mod executor;
mod layout;
mod mutex;
mod reboot;
mod usb;
mod wake_flag;

use crate::{
    executor::Executor,
    matrix::{
        Scanner,
        StatefulScanner,
    },
    usb::{
        hid::HidClass,
        keyboard::Keyboard,
        UsbSink,
    },
    wake_flag::*,
};
use core::fmt::Debug;
use cortex_m_async::mpmc;
use cortex_m_rt as rt;
use embrio_async::embrio_async;
use futures::{
    self,
    future::ready,
    prelude::*,
};
use keebrs::{
    core::{
        Core,
        Role,
    },
    led::NoLeds,
    matrix::{
        Matrix,
        Pull,
        Read,
    },
    serial::Msg,
    translate::Translator,
};
use log::{
    error,
    info,
};
use rtfm::app;
use stm32_usbd::UsbBusType;
use stm32f1xx_futures::{
    self,
    hal::{
        self,
        serial::Serial,
        timer::Timer,
    },
    prelude::*,
    serial,
    stm32::{
        self,
        TIM1,
    },
    timer,
};
use usb_device::{
    bus::UsbBusAllocator,
    device::UsbDeviceBuilder,
    prelude::*,
};

set_allocator!();

const SCANS_PER_SEC: usize = 2_000;
const DEBOUNCE_MS: usize = 5;

const SCAN_KHZ: usize = SCANS_PER_SEC / 1_000;
const DEBOUNCE_CYCLES: usize = SCAN_KHZ * DEBOUNCE_MS;

const VID: u16 = 0x1209;
const PID: u16 = 0x9200;

#[app(device = stm32f1xx_futures::stm32)]
const APP: () = {
    static mut KB_CLASS: HidClass<'static, UsbBusType, Keyboard<NoLeds>> = ();
    static mut USB_DEVICE: UsbDevice<'static, UsbBusType> = ();

    static mut MATRIX: Scanner = ();
    static mut SCAN_REACTOR: timer::Reactor<TIM1> = ();
    static mut LEFT_REACTOR: serial::Reactor<stm32::USART3, [u8; 32]> = ();
    static mut RIGHT_REACTOR: serial::Reactor<stm32::USART1, [u8; 32]> = ();

    static mut USB_FLAG: WakeFlag = WakeFlag::new();

    #[init]
    fn init() -> init::LateResources {
        static mut USB_BUS: Option<UsbBusAllocator<UsbBusType>> = None;

        init_allocator();

        #[cfg(feature = "semihosting")]
        cortex_m_logger::SemihostingLogger::init();

        #[cfg(feature = "itm")]
        cortex_m_logger::ITMLogger::init();

        #[cfg(any(feature = "itm", feature = "semihosting"))]
        log::set_max_level(log::LevelFilter::max());

        let mut flash = device.FLASH.constrain();
        let mut rcc = device.RCC.constrain();

        let clocks = rcc
            .cfgr
            .use_hse(8.mhz())
            .sysclk(72.mhz())
            .pclk1(36.mhz())
            .pclk2(72.mhz())
            .freeze(&mut flash.acr);

        let mut afio = device.AFIO.constrain(&mut rcc.apb2);
        afio.mapr.disable_jtag();

        let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
        let mut gpiob = device.GPIOB.split(&mut rcc.apb2);
        let mut gpioc = device.GPIOC.split(&mut rcc.apb2);

        let mut matrix = build_scanner!(gpioa, gpiob, gpioc);

        let bkp = rcc
            .bkp
            .constrain(device.BKP, &mut rcc.apb1, &mut device.PWR);

        check_bootloader_key(bkp, &mut matrix);

        let tim1 = Timer::tim1(device.TIM1, 2.khz(), clocks, &mut rcc.apb2);
        let scan_reactor = timer::Reactor::new(tim1);

        let bus = UsbBusType::new(
            device.USB,
            (
                gpioa.pa11.into_floating_input(&mut gpioa.crh),
                gpioa.pa12.into_floating_input(&mut gpioa.crh),
            ),
        );

        let reporter = keebrs::report::Reporter::new();

        *USB_BUS = Some(bus);

        let kb_class = HidClass::new(
            Keyboard::new(NoLeds::new(), reporter),
            USB_BUS.as_ref().unwrap(),
        );
        let mut usb_device = UsbDeviceBuilder::new(USB_BUS.as_ref().unwrap(), UsbVidPid(VID, PID))
            .manufacturer("JRC")
            .product("Polymer")
            .serial_number(env!("GIT_VERSION"))
            .build();
        let _ = usb_device.force_reset();

        let right_usart = Serial::usart1(
            device.USART1,
            (
                gpiob.pb6.into_alternate_push_pull(&mut gpiob.crl),
                gpiob.pb7,
            ),
            &mut afio.mapr,
            921_600.bps(),
            clocks,
            &mut rcc.apb2,
        );
        let right_reactor = serial::Reactor::new(right_usart.split());

        let left_usart = Serial::usart3(
            device.USART3,
            (
                gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh),
                gpiob.pb11,
            ),
            &mut afio.mapr,
            921_600.bps(),
            clocks,
            &mut rcc.apb1,
        );
        let left_reactor = serial::Reactor::new(left_usart.split());

        init::LateResources {
            MATRIX: matrix,
            SCAN_REACTOR: scan_reactor,
            LEFT_REACTOR: left_reactor,
            RIGHT_REACTOR: right_reactor,
            USB_DEVICE: usb_device,
            KB_CLASS: kb_class,
        }
    }

    #[idle(resources = [MATRIX, SCAN_REACTOR, LEFT_REACTOR, RIGHT_REACTOR, KB_CLASS, USB_FLAG])]
    #[embrio_async]
    fn idle() -> ! {
        info!("Init finished");

        let scan_timer = resources.SCAN_REACTOR.take();

        let (left_tx, left_rx) = resources.LEFT_REACTOR.take();

        let (mut left_tx, mut left_rx) = (
            Msg::sink(left_tx).sink_map_err(log_error),
            Msg::stream(left_rx).filter_map(filter_errors),
        );

        let (right_tx, right_rx) = resources.RIGHT_REACTOR.take();

        let (mut right_tx, mut right_rx) = (
            Msg::sink(right_tx).sink_map_err(log_error),
            Msg::stream(right_rx).filter_map(filter_errors),
        );

        let scanner = StatefulScanner::with_groups(resources.MATRIX, DEBOUNCE_CYCLES, 5, || {
            cortex_m::asm::delay(5 * 72)
        });

        let mut executor = Executor::new();
        let mut spawner = executor.local_spawner();

        let mut core = Core::new(
            "ortho5x7",
            &["ortho5x7", "ortho5x7"],
            Translator::new(layout::LAYOUT, 2 * 1000),
            mpmc::channel(),
        );

        let usb_sink = UsbSink(resources.KB_CLASS);

        let usb_flag = &mut resources.USB_FLAG;

        let core = async {
            let role = core
                .choose_role(usb_flag.wait(), left_rx.next(), right_rx.next())
                .await;

            info!("role: {:?}", role);

            spawner.spawn(core.run_scanner(scan_timer, scanner));

            match role {
                Role::Center => {
                    let _ = left_tx.send(Msg::Hello).await;
                    let _ = right_tx.send(Msg::Hello).await;
                    spawner.spawn(core.consume_from(left_rx));
                    spawner.spawn(core.consume_from(right_rx));
                    core.translate_to(usb_sink).await;
                }
                Role::Left => {
                    let _ = left_tx.send(Msg::Hello).await;
                    spawner.spawn(core.consume_from(left_rx));
                    core.forward_to(right_tx).await;
                }
                Role::Right => {
                    let _ = right_tx.send(Msg::Hello).await;
                    spawner.spawn(core.consume_from(right_rx));
                    core.forward_to(left_tx).await;
                }
            }

            error!("uh oh, shouldn't get here");
        };

        executor.spawn(core);

        info!("starting executor");

        loop {
            executor.run();
        }
    }

    #[interrupt(resources = [USB_DEVICE, KB_CLASS, USB_FLAG])]
    fn USB_HP_CAN_TX() {
        let dev = &mut *resources.USB_DEVICE;
        let class = &mut *resources.KB_CLASS;
        if dev.poll(&mut [class]) {
            resources.USB_FLAG.wake();
        }
    }

    #[interrupt(resources = [USB_DEVICE, KB_CLASS, USB_FLAG])]
    fn USB_LP_CAN_RX0() {
        let dev = &mut *resources.USB_DEVICE;
        let class = &mut *resources.KB_CLASS;
        if dev.poll(&mut [class]) {
            resources.USB_FLAG.wake();
        }
    }

    #[interrupt(resources = [RIGHT_REACTOR])]
    fn USART1() {
        resources.RIGHT_REACTOR.turn()
    }

    #[interrupt(resources = [LEFT_REACTOR])]
    fn USART3() {
        resources.LEFT_REACTOR.turn()
    }

    #[interrupt(resources = [SCAN_REACTOR])]
    fn TIM1_UP() {
        resources.SCAN_REACTOR.turn()
    }
};

fn check_bootloader_key<M: Pull + Read>(bkp: hal::backup_domain::BackupDomain, matrix: &mut M) {
    matrix.pull(0);
    cortex_m::asm::delay(5 * 72);
    let reboot = matrix.read(0);
    matrix.release(0);

    if reboot {
        reboot::reboot_bootloader(bkp);
    }
}

fn log_error<E>(_e: E) -> ()
where
    E: Debug,
{
    error!("{:?}", _e);
}

fn filter_errors<T, E>(res: Result<T, E>) -> futures::future::Ready<Option<T>>
where
    T: Debug,
    E: Debug,
{
    ready(match res {
        Ok(ok) => Some(ok),
        Err(e) => {
            log_error(e);
            None
        }
    })
}
